#include <stdio.h>

// Fungsi untuk menghitung faktorial
int factorial(int n) {
    if (n <= 1)
        return 1;
    else
        return n * factorial(n - 1);
}

// Fungsi untuk menghitung kombinasi
int combination(int n, int r) {
    return factorial(n) / (factorial(r) * factorial(n - r));
}

int main(){
    int n; 
    printf("Masukkan N : ");
    scanf("%d", &n);

   for (int i = 0; i < n; i++) {
        for (int j = 0; j <= n - i - 2; j++) {
            printf(" ");
        }
        for (int j = 0; j <= i; j++) {
            printf("%d ", combination(i, j));
        }
        printf("\n");
    } 

  return 0;
}
