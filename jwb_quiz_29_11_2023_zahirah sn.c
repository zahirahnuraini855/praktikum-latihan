#include <stdio.h>

int main() {
   int n, i;
   float nilai,total=0;

    printf("Masukkan jumlah data: ");
    scanf("%d", &n);

    for (i = 1; i <= n; ++i){
        printf("Masukkan Data ke %d : ", i);
        scanf("%f", &nilai);
        total += nilai;
    }
   float rata = total / n;
   printf("\nBanyaknya Data :%d\n",n);
   printf("Total Nilai Data : %.2f\n", total);
   printf("Rata-rata nilai Data : %.2f\n", rata);


    return 0;
}
