#include <stdio.h>
#include <string.h>

void jdl_aplikasi()
{
    printf("*************************\n");
    printf("*  Aplikasi Hitung Gaji *\n");
    printf("*************************\n\n");

}

void gapok_tunja(char gol, char *status, float *gapok, float *tunja)
{
    switch (gol)
    {
    case 'A':
        gapok = 200000.0;
        if (strcmp(status, "Nikah") == 0)
        {
            tunja = 50000.0;
        }
        else
        {
            if (strcmp(status, "Belum") == 0)
            {
                tunja = 25000.0;
            }
        }
        break;
    case 'B':
        gapok = 350000.0;
        if (strcmp(status, "Nikah") == 0)
        {
            tunja = 75000.0;
        }
        else
        {
            if (strcmp(status, "Belum") == 0)
            {
                tunja = 60000.0;
            }
        }
        break;
    default:
        printf(" Salah Input Golongan !!!\n");
        break;
    }


}

float prosen_potongan(float gapok)
{
    float prosen_pot = 0.0;

    return prosen_pot;
}

float potongan(float gapok, float tunja, float prosen_pot)
{
    float pot = 0.0;

    return pot;
}

float gaji_bersih(float gapok, float tunja, float pot)
{
    float gaber = 0.0;

    return gaber;
}

void input(char *nama, char *gol, char *status)
{
    printf("Nama Karyawan       : ");
    scanf("%s", nama);
    printf("Golongan (A\\B)      : ");
    scanf(" %c", &gol);
    printf("Satus (Nikah\\Belum) : ");
    scanf("%s", status);

}

void output(float gapok, float tunja, float pot, float gaber)
{
   
    printf("Gaji Pokok     : Rp.%.2f\n", gapok);
    printf("Tunjangan      : Rp.%.2f\n", tunja);
    printf("Potongan Iuran : Rp.%.2f\n", pot);
    printf("Gaji Bersih    : Rp.%.2f\n", gaber);

}
